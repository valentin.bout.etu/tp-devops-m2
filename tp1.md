# TP : Mise en place d'un environnement CI/CD avec Terraform, Ansible et Gitlab CI pour le déploiement automatique d'une application web

## Introduction
Dans le monde de la gestion des infrastructures et du développement logiciel, l'approche DevOps a gagné en importance en tant que pratique visant à améliorer la collaboration entre les équipes de développement et d'exploitation. L'automatisation joue un rôle essentiel dans cette approche, en permettant de déployer rapidement et de manière cohérente des applications sur des infrastructures diverses. Ce TP vise à introduire les étudiants à la mise en place d'un environnement DevOps en utilisant Terraform, Ansible et Gitlab CI pour déployer automatiquement une application web sur des machines virtuelles.

## Objectifs du TP
1. Comprendre les principes de base de DevOps et l'importance de l'automatisation dans le cycle de développement.
2. Apprendre à utiliser Terraform pour créer une infrastructure cloud (machines virtuelles) de manière automatisée.
3. Utiliser Ansible pour configurer les machines virtuelles et les préparer à l'exécution de l'application.
4. Mettre en place un pipeline d'intégration continue (CI) avec Gitlab CI pour automatiser le déploiement de l'application web.

## Instructions

### 1. Mise en place de l'infrastructure avec Terraform
   - Créez un répertoire de projet Terraform.
   - Écrivez les fichiers de configuration Terraform nécessaires pour créer des machines virtuelles.
   - Définissez des variables pour personnaliser la taille, le type et le nombre de machines virtuelles.
   - Documentez votre configuration Terraform.

### 2. Configuration des machines virtuelles avec Ansible
   - Créez un répertoire de projet Ansible.
   - Rédigez des playbooks Ansible pour installer les dépendances requises sur les machines virtuelles.
   - Configurez les machines virtuelles pour accueillir votre application web.
   - Documentez vos playbooks Ansible.

### 3. Développement de l'application web
   - Écrivez une petite application web dans le langage de votre choix.
   - Hébergez le code source de l'application sur un référentiel Gitlab.

### 4. Mise en place du pipeline CI/CD avec Gitlab CI
   - Créez un fichier `.gitlab-ci.yml` dans le référentiel de votre application pour définir les étapes du pipeline.
   - Configurez le pipeline pour déployer automatiquement l'application sur les machines virtuelles créées avec Terraform en utilisant Ansible.
   - Utilisez des variables Gitlab pour stocker les informations sensibles (par exemple, les clés d'accès) de manière sécurisée.
   - Documentez le fichier `.gitlab-ci.yml` et expliquez comment il fonctionne.

### 5. Testez le pipeline
   - Apportez des modifications à votre application web pour déclencher le pipeline CI/CD.
   - Assurez-vous que l'application est déployée automatiquement sur les machines virtuelles.

### 6. Rapport
   - Rédigez un rapport décrivant les étapes que vous avez suivies pour mettre en place l'environnement CI/CD.
   - Incluez des captures d'écran et des explications pour chaque étape.
   - Réfléchissez sur les avantages de l'automatisation dans le contexte DevOps.

## Rapport

Tout d'abord, j'ai commence par creer les machines utiles pour deployer et servir l'application web finale.
J'ai utilise terraform (dans le dossier terraform) permettant de creer des machines de deploiement ainsi qu'un serveur d'aide nomme alfred.
Terraform construit automatiquement mon inventaire ansible.
Cette inventaire ansible permet notamment d'installer docker et inscrire les runners de gitlab. Ces runners sont utiles pour pouvoir deployer et communiquer avec les machines de production uniquement accessible sur le reseaux de l'universite.
Ansible permet donc de creer et installer les dependances necesssaire.
Finalement, le dossier application contient notre application et un simple docker nginx. Ce nginx sers le dossier html.
Finalement, notre ci/cd construit l'image de notre application en phase de build sur nos runners pour ensuite les deployer en ssh en se connectant a l'ip du serveur de production.

### Limitation

Nous pourrions ameliorer l'automatisation de notr eprojet en provisionnant une machine servant de coeur a ansible. Cette machine s'occuperait du deploiement vers les serveurs de production. Cela eviterait notamment de devoir mettre a jour les variables de la CI/CD par exemple, bien que l'on pourra ausis le faire dans la ci/cd contenant le projet terraform par exemple.

### Avantage

Notre code peux maintenant etre deployer sans intervention humaine et sans arret. Nous n'avons plus beosin de realiser des backups des machines (seulement de leur contenu s'il le faut comme les bases de donness) car nous avons une infra as code efficace.
