data "openstack_compute_flavor_v2" "puissante" {
  #name  = "flavor-1vcpu-512ram-10GoDisk"
  ram   = "4096"
  vcpus = "2"
  disk  = "20"
}

data "openstack_compute_flavor_v2" "trespuissante" {
  #name  = "flavor-1vcpu-512ram-10GoDisk"
  ram   = "8192"
  vcpus = "4"
  disk  = "40"
}

data "openstack_compute_flavor_v2" "normal" {
  #name  = "flavor-1vcpu-512ram-10GoDisk"
  ram   = "1024"
  vcpus = "1"
  disk  = "10"
}
