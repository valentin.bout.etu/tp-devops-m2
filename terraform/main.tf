terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.51.1"
    }
  }
}

provider "openstack" {
    user_name = var.openstack_user_name
    tenant_name = var.openstack_tenant
    password = var.openstack_password
    auth_url = var.openstack_auth_url
}

resource "openstack_compute_instance_v2" "instances" {
  count = var.openstack_instances_count
  name = "main"
  image_id = data.openstack_images_image_v2.ubuntu22.id
  flavor_id = data.openstack_compute_flavor_v2.puissante.id
  key_pair        = "keypair-tf"
  security_groups = ["default"]

  metadata = {
    group = "main"
  }

}

resource "openstack_compute_instance_v2" "alfred_my_friend" {
  count = var.openstack_instances_gitlab_count
  name = "alfred_my_friend"
  image_id = data.openstack_images_image_v2.ubuntu22.id
  flavor_id = data.openstack_compute_flavor_v2.normal.id
  key_pair = "keypair-tf"
  security_groups = ["default"]

  metadata = {
    group = "alfred_my_friend"
  }
}
