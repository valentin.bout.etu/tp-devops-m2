data "openstack_images_image_v2" "ubuntu22" {
  name        = "ubuntu22.04"
  most_recent = true
}

data "openstack_images_image_v2" "centos7" {
  name        = "centos-7"
  most_recent = true
}
