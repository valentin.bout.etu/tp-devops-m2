variable openstack_instances_count {
  type        = number
  default     = 1
  description = "Number of default terraform to span"
}

variable openstack_instances_gitlab_count {
  type        = number
  default     = 1
  description = "Number of gitlab worker"
}

