variable "openstack_user_name" {
    type = string
}

variable "openstack_tenant" {
    type = string
}

variable "openstack_password" {
    type = string
    sensitive = true
}

variable "openstack_auth_url" {
    type = string
}

variable "keypair_public" {
    type = string
}