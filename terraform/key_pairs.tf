# Retrieve ssh key
resource "openstack_compute_keypair_v2" "keypair" {
  name = "keypair-tf"
  public_key = var.keypair_public
}
