locals {
    main_addrs = [
        for inst in openstack_compute_instance_v2.instances :
        inst.access_ip_v4
    ]
}

locals {
    alfred_my_friend_addrs = [
        for inst in openstack_compute_instance_v2.alfred_my_friend :
        inst.access_ip_v4
    ]
}

resource "local_file" "inventory_ansible" {
    content = templatefile("inventories/inventory.tftpl", {
        main_addrs = local.main_addrs
        alfred_my_friend_addrs = local.alfred_my_friend_addrs
        ubuntu_user = "ubuntu"
        ubuntu_pri_key = "/home/valentin/Documents/DevOps/tp-devops-m2/terraform/.ssh/keypair-tf"
    })
    filename = "${path.root}/../ansible/inventory"
    file_permission      = "0644"
    directory_permission = "0700"
}