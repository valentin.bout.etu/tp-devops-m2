import subprocess
import argparse

parser = argparse.ArgumentParser(prog='Almost Docker', description='Create a container and run the program given')
parser.add_argument('program', nargs=1)
parser.add_argument('--cpu', default="512", dest="cpu")
parser.add_argument('--memory', default="104857600", dest="memory")

args = parser.parse_args()
# Create c group for controlling memory and cpu
subprocess.run(["mkdir", "-p", "/sys/fs/cgroup/memory,cpu"])
subprocess.run(["mkdir", "-p", "/sys/fs/cgroup/memory,cpu/my_container"])

# Config limit for the container
subprocess.run("echo " + str(args.cpu) + " > /sys/fs/cgroup/memory,cpu/my_container/cpu.shares", shell=True)
subprocess.run("echo " + str(args.memory) + "> /sys/fs/cgroup/memory,cpu/my_container/memory.limit_in_bytes", shell=True)



# Run the program in the cgroup
subprocess.run(["sh", "-c", "echo $$ > /sys/fs/cgroup/memory,cpu/my_container/cgroup.procs && " + " ".join(args.program)])

