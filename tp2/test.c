#include <stdlib.h>
#include <unistd.h>

int main(void)
{
    char str[] = "Memory exhausted";
    while (1)
    {
        void *ptr = malloc(1024*1024); /* Allocate all the memory */
        if (ptr == NULL) /* Memory exhausted */
        {
            write(2, str, sizeof(str)); /* Write the error message
                (using write to avoid internal printf memory allocation) */
            while (1)
                continue;
        }
    }
    return 0;
}
