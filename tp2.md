# Création d'un composant de conteneurisation simplifié en autonomie

## Documentation préliminaire

1. Qu'est-ce que cgroups (Control Groups) dans le contexte des systèmes Linux ? 
- Décrivez en quoi consistent les cgroups et pourquoi ils sont utilisés.
Les cgroups sont utilises pour limiter ou controler les ressources utilises par les process. Ils representent des groupes de process organiser selon une hierarchie, et pouvant appartenir a plusieurs cgroups.
2. Pourquoi les cgroups sont-ils importants dans la conteneurisation ? 
- Expliquez comment les cgroups contribuent à l'isolation des ressources entre les conteneurs.
Les cgroups contribuent a l'isolation des resources car nous pouvons controller les ressources max utilises par un conteneurs. Cela permet notamment d'eviter qu'un conteneur prenne toute la memoire de son host, n'en laissant plus pour les autres. Cela permets aussi de restreindre l'acces aux appareils pouvant etre connecter sur l'host et monter sur certain container. Les cgroups peuvent aussi etre utiliser pour prioriser certain process que d'autre.
3. Quels sont les principaux sous-systèmes de cgroups et à quoi servent-ils ? 
- Nommez quelques-uns des sous-systèmes de cgroups et expliquez brièvement leurs rôles.
CPU : Permet de controller le partage de l'utilisation du CPU seulement si le CPU est a son usage maximal.
Memory : Controle et limite la memoire (RAM, SWAP) que peux utiliser un process
devices : Controle les process pouvant creer, ouvrir, lire et detruire les devices connecte a l'host.
pids : Limite le nombre de sous process que peux creer un process.
4. Comment pouvez-vous utiliser les cgroups pour limiter la quantité de CPU qu'un conteneur peut utiliser ? 
- Détaillez les étapes pour définir des limites CPU à l'aide des cgroups.
```sh
cd /tmp # Creating in tmp folder
mkdir cgroup
mount -t cgroup -o cpu cgroup cgroup
cd cgroup
mkdir my_app_group_cpu
echo "100" > cpu.shares
echo 0 > cgroup.procs
```
5. Qu'est-ce que le NAT (Network Address Translation) et comment est-il utilisé dans la gestion du réseau des conteneurs ? 
- Expliquez en quoi consiste le NAT et son rôle dans la gestion des communications réseau des conteneurs.
Le nat permet a un appareil tel qu'un routeur de traduire une IP public vers un reseau d'IP prive et inversement. Il existe plusieurs types de NAT (statique, dynamique et PAT). Cela permets ainsi a un appareil sur un reseaux prive de communiquer vers des reseaux public tel qu'internet. Le nat creer une table de correspondance et de ports associant chacun des ces ports de ses ip publics vers un autre port + ip pour une machine vers le reseaux prive.

6. Comment pouvez-vous configurer une règle NAT pour permettre à un conteneur d'accéder à Internet depuis un réseau isolé ? 
```sh
# Creation d'une interface virtuel
$ ip link add eth_container type dummy
# Add ip address network to eth_container
$ ip addr add 10.0.0.1/8 dev eth_container
# Allow forwarding in iptables (avec eth0 connecte a internet / autre reseau)
$ iptables -A FORWARD -i dummy -o eth0 -s 10.0.0.0/8 -j ACCEPT
$ iptables -A FORWARD -i eth0 -o dummy -m state --state ESTABLISHED,RELATED -j ACCEPT
# Enable port forwarding on kernel
$ echo "net.ipv4.ip_forward=1" > /etc/sysctl.conf
$ sysctl -p && sysctl --system
# Enable NAT
$ iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
```
7. Quelles sont les ressources en ligne, les articles ou les tutoriels que vous avez trouvés utiles pour comprendre les cgroups et la gestion du réseau des conteneurs ?
[1](https://zarak.fr/linux/exploration-des-cgroups/)
[2](https://man7.org/linux/man-pages/man7/cgroups.7.html)
[3](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/resource_management_guide/sec-cpu)
[4](https://www.manageengine.com/fr/network-configuration-manager/configlets/what-is-nat.html)

##  Création d'un environnement isolé
1. Choix du langage de programmation 
Python sera utilise pour creer les scripts. Cela permets de pouvoir faciliter le developpement des parametres en lignes de commandes, executes des commandes sur l'OS facilement sans avoir a gerer la complexite du C ou du GO.

2. Création d'un groupe de cgroups
-  Familiarisez vous avec les commandes cgroups telles que `cgcreate`, `cgset`, `cgexec`, etc.
3. Création d'un groupe de cgroup
```python
import subprocess
subprocess.run(["cgcreate", "-g", "memory,cpu:/my_container"])
```
4. Développement du programme d'isolation
-  Développer un programme qui effectue les tâches suivantes :
* Crée un sous-groupe de cgroups pour le processu.
* Configure des limites CPU et mémoire pour ce sous-groupe.
* Exécute un processus (par exemple, un script Python) au sein de ce sous-groupe.
5. Test du programme
- Tester le programme en exécutant un processus gourmand en ressources (par exemple, une boucle infinie) à l'intérieur du sous-groupe.
- Observer que le processus est limité en termes d'utilisation CPU et mémoire.

## Création d'un environnement de conteneur simple
1. Intégration des programmes
- Modifier votre/vos programme(s) pour les intégrer en un seul script ou un ensemble de scripts qui gère à la fois l'isolation des ressources (cgroups) et la gestion du réseau.
- Les programmes devraient être capables de créer un environnement de conteneur simple.
2. Configuration de l'environnement de conteneur
- Configurer votre environnement de conteneur en définissant des limites CPU et mémoire à l'aide des cgroups, en créant une interface réseau virtuelle pour le conteneur et en configurant une règle NAT pour l'accès à Internet.
3. Test de l'environnement de conteneur
-  Tester votre environnement de conteneur en exécutant un processus (par exemple, un serveur web) à l'intérieur du conteneur.
- Vérifiez que le processus du conteneur est correctement isolé en termes de ressources CPU et mémoire, et qu'il peut accéder à Internet.
